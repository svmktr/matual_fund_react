import React, { Component } from 'react'
import { Layout } from 'antd';
import { Card, Col, Row } from 'antd';
import { Button } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
const { Header, Footer, Sider, Content } = Layout;


export class Getdata extends Component {

  state = {
    // funds: Array(100),
    fundDetail: null,
    loading: false

  }


  componentDidMount = async () => {
    const code = this.props.match.params.code
    const url = `https://api.kuvera.in/api/v3/funds/${code}.json`
    await fetch(url).then(data => {
      data.json()
        .then(data =>
          this.setState({ fundDetail: data[0], loading: true })
        )
    })


  }


  goBack = () => {
    this.props.history.push('/')
  }

  // text = () =>{
  //     console.log('Hello');
  // }

  render() {
    // console.log(this.state.fundDetail.code);
    let detail
    if (this.state.loading) {
      // console.log(this.state.fundDetail.aum);
      detail = <div className="site-card-wrapper" style={{ display: 'grid', gridGap: '16px' }}>
        <Row gutter={16}>
          <Col span={6}>
            <Card title="Name" bordered={false}>
              {this.state.fundDetail.name}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Reinvestment" bordered={false}>
              {this.state.fundDetail.fund_type}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Fund_Category" bordered={false}>
              {this.state.fundDetail.fund_category}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Plan" bordered={false}>
              {this.state.fundDetail.plan}
            </Card>
          </Col>
        </Row>


        <Row gutter={16}>
          <Col span={6}>
            <Card title="ISIN" bordered={false}>
              {this.state.fundDetail.ISIN}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="aum" bordered={false}>
              {this.state.fundDetail.aum}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Lump_Available" bordered={false}>
              {this.state.fundDetail.lump_available}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Crisil_Rating" bordered={false}>
              {this.state.fundDetail.crisil_rating}
            </Card>
          </Col>
        </Row>


        <Row gutter={16}>
          <Col span={6}>
            <Card title="Expense_Ratio" bordered={false}>
              {this.state.fundDetail.expense_ratio}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Expense_Ratio_Date" bordered={false}>
              {this.state.fundDetail.expense_ratio_date}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Face_Value" bordered={false}>
              {this.state.fundDetail.face_value}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Fund_House" bordered={false}>
              {this.state.fundDetail.fund_house}
            </Card>
          </Col>
        </Row>


        <Row gutter={16}>
          <Col span={6}>
            <Card title="Maturity_Type" bordered={false}>
              {this.state.fundDetail.maturity_type}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Portfolio_Turnover" bordered={false}>
              {this.state.fundDetail.portfolio_turnover}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Start_Date" bordered={false}>
              {this.state.fundDetail.start_date}
            </Card>
          </Col>
          <Col span={6}>
            <Card title="Fund_Manager" bordered={false}>
              {this.state.fundDetail.fund_manager}
            </Card>
          </Col>
        </Row>
      </div>
    }
    else {
      detail = <LoadingOutlined />
    }

    return (
      <div>
        <Layout>
          <Header>  <h3 style={{ textAlign: 'center', color: '#f5f5f5' }}>Fund Detail</h3>

          </Header>
          <Content style={{ marginTop: '10px', marginRight: '5px' }}>
            <div>{detail}</div>
          </Content>
          <Footer>
            <Button type="primary" onClick={this.goBack}>Back</Button>
          </Footer>
        </Layout>
      </div>
    )
  }
}

export default Getdata
