import React, { Component } from 'react'
import Table from './TableComponent'
import { Layout } from 'antd';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import FuzzySearch from 'fuzzy-search'
import { Input } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
const { Search } = Input;
const { Header, Footer, Sider, Content } = Layout;



export class Getdata extends Component {
    state = {
        funds: Array(100),
        myFunds: null,
        filterData: null,
        searchedData: null
    }

    componentDidMount = async () => {
        const url = 'https://api.kuvera.in/api/v3/funds.json'
        await fetch(url).then(data => {
            data.json()
                .then(data =>
                    this.setState({ funds: data.splice(0, 100) })
                )
                .then(() => this.filterData())
        })
    }


    filterData = () => {
        const myData = []
        this.state.funds.map(data => {
            const newStore = {
                code: data.code,
                name: data.name,
                fund_category: data.fund_category,
                fund_type: data.fund_type,
                plan: data.plan,
                return_year_1: data.returns.year_1,
                return_year_3: data.returns.year_3
            }
            myData.push(newStore)
        })
        this.setState({ myFunds: myData })
    }


    filterTable = (dataType, data) => {
        let filteredData = this.state.myFunds.filter(item => {
            return item[dataType] === data
        })
        this.setState({ filterData: filteredData })
    }


    onChange = (value) => {
        const searcher = new FuzzySearch(this.state.myFunds, ['name'], {
            caseSensitive: false
        });
        const result = searcher.search(value);
        this.setState({
            searchedData: result
        });
    }



    render() {

        let dataType = {
            plan: [],
            fund_category: [],
            fund_type: []
        }


        let x = (this.state.myFunds || []).map(item => {
            if (!dataType.plan.includes(item.plan)) {
                dataType.plan.push(item.plan)
            }
            if (!dataType.fund_category.includes(item.fund_category)) {
                dataType.fund_category.push(item.fund_category)
            }
            if (!dataType.fund_type.includes(item.fund_type)) {
                dataType.fund_type.push(item.fund_type)
            }
        })


        let tableData
        let table
        if (this.state.myFunds) {
            tableData = this.state.myFunds
            if (this.state.filterData) {
                tableData = this.state.filterData
            }
            else if (this.state.searchedData) {
                tableData = this.state.searchedData
            }
            table = <Table tableData={tableData} />
        }
        else {
            table = <LoadingOutlined />
        }


        const { SubMenu } = Menu
        const menu = (
            <Menu>
                <SubMenu title="Fund Category  .">
                    {dataType.fund_category.map((item, key) => (
                        <Menu.Item key={key}>
                            <a target="_blank" onClick={() => this.filterTable('fund_category', item)}>
                                {item} </a>
                        </Menu.Item>
                    ))}
                </SubMenu>

                <SubMenu title=" Plan">
                    {dataType.plan.map((item, key) => (
                        <Menu.Item key={key}>
                            <a target="_blank" onClick={() => this.filterTable('plan', item)}>
                                {item} </a>
                        </Menu.Item>
                    ))}
                </SubMenu>

                <SubMenu title=" Fund Type">
                    {dataType.fund_type.map((item, key) => (
                        <Menu.Item key={key}>
                            <a target="_blank" onClick={() => this.filterTable('fund_type', item)}>
                                {item} </a>
                        </Menu.Item>
                    ))}
                </SubMenu>

            </Menu>
        );



        return (
            <div>
                <Layout>
                    <Header style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Dropdown overlay={menu}>
                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                Filter Table <DownOutlined /></a>
                        </Dropdown>
                        <h3 style={{ textAlign: 'center', color: '#f5f5f5' }}>Matual Fund Data</h3>
                        <Search
                            placeholder="Fund Name"
                            onChange={e => this.onChange(e.target.value)}
                            style={{ width: 200, height: 30 }}
                        />
                    </Header>
                    <Content>
                        {table}
                    </Content>
                </Layout>
            </div>
        )
    }
}

export default Getdata
