import React, { Component } from 'react'
import { Table } from 'antd';
import { Link } from 'react-router-dom';
export class TableComponent extends Component {
    render() {
        const tableData = this.props.tableData
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'code',
                sorter: (a, b) => a.name.localeCompare(b.name),
                render: (text, record) => <Link to={`/detail_info/${record.code}`}>{text}</Link>,

            },
            {
                title: 'Fund Category',
                dataIndex: 'fund_category',
                key: 'code',
                sorter: (a, b) => a.fund_category.localeCompare(b.fund_category),

            },
            {
                title: 'Fund Type',
                dataIndex: 'fund_type',
                key: 'code',
                sorter: (a, b) => a.fund_type.localeCompare(b.fund_type),
            },
            {
                title: 'Plan',
                dataIndex: 'plan',
                key: 'code',
                sorter: (a, b) => a.plan.localeCompare(b.plan)
            },

            {
                title: 'Year_1 Return',
                dataIndex: 'return_year_1',
                key: 'code',
                sorter: {
                    compare: (a, b) => a.return_year_1 - b.return_year_1,
                }
            },
            {
                title: 'Year_3 Return',
                dataIndex: 'return_year_3',
                key: 'code',
                sorter: {
                    compare: (a, b) => a.return_year_1 - b.return_year_1,
                }
            },

        ];

        return (
            <div>
                <Table columns={columns} dataSource={tableData} />
            </div>
        )
    }
}

export default TableComponent
